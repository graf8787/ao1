package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

@Slf4j
public class CsvReader {
    private final Reader reader;
    private final CSVParser parser;

    public CsvReader(URL url) throws IOException {
        reader = new InputStreamReader(new BOMInputStream(url.openStream()), "UTF-8");
        parser = new CSVParser(reader, CSVFormat.DEFAULT);
    }

    public Entity getEntity() throws IOException {
        if (parser.iterator().hasNext()) {
            final CSVRecord record = parser.iterator().next();
            final int id = Integer.parseInt(record.get(0));
            final String name = record.get(1);
            final String condition = record.get(2);
            final String state = record.get(3);
            final float price = Float.parseFloat(record.get(4));
            final Entity entity = new Entity(id, name, condition, state, price);
            if (LOGGER.isInfoEnabled())
                LOGGER.info(entity.toString());
            return entity;
        } else {
            parser.close();
            reader.close();
            if (LOGGER.isInfoEnabled())
                LOGGER.info("Файл закрыт");
            return null;
        }
    }

}
