package ru.graf8787.fun.ao1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Entity implements Comparable<Entity> {

    private final int id;
    private final String name;
    private final String condition;
    private final String state;
    private final float price;

    /**
     * Необычненько так из-за того, что иначе три сэт посносит дубликаты
     *
     * @param entity
     * @return
     */
    public int compareTo(Entity entity) {
        final int compare = Float.compare(entity.price, this.price);
        if (compare != 0)
            return compare;
        else
            return this == entity ? 0 : -1; // TODO эта еденица в случае с оромным колическтвом идентичных элементов может
        // повысить сложность за счет того что дерево выстрится в линкедлист. Это можно решить выставляя здесь попеременно 1 / -1
        // но реализовывать не хочется.
    }


}
