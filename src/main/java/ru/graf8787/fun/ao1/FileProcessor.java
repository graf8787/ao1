package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;

@Slf4j
public class FileProcessor implements Runnable {

    private URL url;
    private ResultHandler resultHandler;

    public FileProcessor(ResultHandler resultHandler, URL url) {
        this.resultHandler = resultHandler;
        this.url = url;
    }

    @Override
    public void run() {
        try {
            CsvReader csvReader = new CsvReader(url);
            Entity entity;
            do {
                entity = csvReader.getEntity();
                if (entity != null) {
                    synchronized (resultHandler) {
                        resultHandler.tryAdd(entity);
                    }
                }
            } while (entity != null);
            LOGGER.info("Файл обработан {}", url);
        } catch (IOException e) {
            LOGGER.error("Ошибка при работе с файлом", e);
        }
    }

}
