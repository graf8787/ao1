package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MainService {

    public Set<Entity> execute(List<URL> files, int threadCount) {
        final ExecutorService service = Executors.newFixedThreadPool(threadCount);
        final ResultHandler resultHandler = new ResultHandler();
        for (URL file : files) {
            service.submit(new FileProcessor(resultHandler, file));
        }
        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("Не могу дождаться конца", e);
        }
        return resultHandler.result();
    }

}
