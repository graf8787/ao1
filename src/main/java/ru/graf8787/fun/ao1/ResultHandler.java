package ru.graf8787.fun.ao1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ResultHandler {

    private final TreeSet<Entity> result;
    private final HashMap<Integer, Integer> keyCount;
    private final int maxSize;
    private final int unique;
    private float maxValue;

    public ResultHandler(int maxSize, int unique) {
        this.maxSize = maxSize;
        this.unique = unique;
        this.result = new TreeSet();
        this.keyCount = new HashMap<>();
        this.maxValue = Float.MAX_VALUE;
    }

    public ResultHandler() {
        this(1000, 20);
    }

    public void tryAdd(Entity entity) {
        final int countWithSameId = safe(keyCount.get(entity.getId()));
        if (countWithSameId == unique) {

            final Entity sameId = withSameId(entity);
            if (Float.compare(sameId.getPrice(), entity.getPrice()) > 0) {
                result.remove(sameId);

                final Entity first = result.first();
                maxValue = Math.min(maxValue, first.getPrice());

                result.add(entity);
            }

        } else if (result.size() < maxSize) {

            result.add(entity);
            keyCount.put(entity.getId(), countWithSameId + 1);
            maxValue = result.first().getPrice();

        } else if (Float.compare(maxValue, entity.getPrice()) > 0) {

            final Entity first = result.first();
            result.remove(first);
            keyCount.put(first.getId(), keyCount.get(first.getId()) - 1);

            final Entity second = result.first();
            maxValue = Math.min(maxValue, second.getPrice());

            result.add(entity);
            keyCount.put(entity.getId(), countWithSameId + 1);
        }
    }

    private int safe(Integer value) {
        return value == null ? 0 : value;
    }

    private Entity withSameId(final Entity entity) {
        final Iterator<Entity> iterator = result.iterator();
        while (iterator.hasNext()) {
            final Entity next = iterator.next();
            if (next.getId() == entity.getId()) {
                return next;
            }
        }
        throw new RuntimeException();
    }

    public Set<Entity> result() {
        return result;
    }

}
