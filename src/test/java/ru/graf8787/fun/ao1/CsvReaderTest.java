package ru.graf8787.fun.ao1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CsvReaderTest {

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testGetEntity() throws IOException {
        CsvReader csvReader = new CsvReader(CsvReaderTest.class.getClassLoader().getResource("simple.csv"));
        assertEquals(csvReader.getEntity().toString(), new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 10.0f).toString());
        assertEquals(csvReader.getEntity().toString(), new Entity(2, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f).toString());
        assertEquals(csvReader.getEntity().toString(), new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 15.0f).toString());
        assertEquals(csvReader.getEntity().toString(), new Entity(3, "Name (string),  ", "Condition (string), ", "State (string), ", 50.0f).toString());
        assertNull(csvReader.getEntity());
        assertNull(csvReader.getEntity());
    }
}