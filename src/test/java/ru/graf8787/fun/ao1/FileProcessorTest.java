package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import static org.testng.Assert.assertEquals;

@Slf4j
public class FileProcessorTest {

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testRun() {

        final TreeSet<Entity> expected = new TreeSet();

        final ResultHandler resultHandler = new ResultHandler(2, 1);
        final URL resource = CsvReaderTest.class.getClassLoader().getResource("simple.csv");

        final FileProcessor fileProcessor = new FileProcessor(resultHandler, resource);
        fileProcessor.run();

        final Set<Entity> result = resultHandler.result();
        for (Entity e : result) {
            LOGGER.info(e.toString());
        }

        Entity[] good = {
                new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 10.0f),
                new Entity(2, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f)
        };
        for (Entity e : good) {
            expected.add(e);
        }
        assertEquals(Arrays.toString(result.toArray()), Arrays.toString(expected.toArray()));
    }
}