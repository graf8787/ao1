package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.testng.Assert.assertEquals;

@Slf4j
public class MainServiceTest {

    private MainService mainService;

    @BeforeMethod
    public void setUp() {
        mainService = new MainService();
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testExecute() {
        final URL resource = CsvReaderTest.class.getClassLoader().getResource("simple.csv");
        final List<URL> urls = new ArrayList<>(25);
        for (int i = 0; i < 25; i++) {
            urls.add(resource);
        }
        getExecute(urls, 4);
        getExecute(urls, 2);
        getExecute(urls, 1);
    }

    private Set<Entity> getExecute(List<URL> urls, int threadCount) {
        final long currentTimeMillis = System.currentTimeMillis();
        final Set<Entity> execute = mainService.execute(urls, threadCount);
        assertEquals(execute.size(), 60); // потому что в файле 3 уникальных ИД, а дублей может быть 20
        LOGGER.info("Выполнено за {} c {} потоками", System.currentTimeMillis() - currentTimeMillis, threadCount);
        return execute;
    }
}