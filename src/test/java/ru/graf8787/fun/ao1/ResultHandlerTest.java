package ru.graf8787.fun.ao1;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import static org.testng.Assert.assertEquals;

@Slf4j
public class ResultHandlerTest {

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testTryAdd() {
        final ResultHandler resultHandler = new ResultHandler(5, 2);
        final TreeSet<Entity> expected = new TreeSet();

        Entity[] bad = {
                new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 14.0f),
                new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f),
                new Entity(2, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f),
                new Entity(4, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f),
                new Entity(5, "Name (string),  ", "Condition (string), ", "State (string), ", 20.0f),
        };
        Entity[] good = {
                new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 10.0f),
                new Entity(1, "Name (string),  ", "Condition (string), ", "State (string), ", 11.0f),
                new Entity(2, "Name (string),  ", "Condition (string), ", "State (string), ", 12.0f),
                new Entity(3, "Name (string),  ", "Condition (string), ", "State (string), ", 13.0f),
                new Entity(4, "Name (string),  ", "Condition (string), ", "State (string), ", 14.0f),
        };

        for (Entity e : bad) {
            resultHandler.tryAdd(e);
        }
        for (Entity e : good) {
            resultHandler.tryAdd(e);
            expected.add(e);
        }

        final Set<Entity> result = resultHandler.result();
        for (Entity e : result) {
            LOGGER.info(e.toString());
        }
        assertEquals(Arrays.toString(result.toArray()), Arrays.toString(expected.toArray()));
    }
}